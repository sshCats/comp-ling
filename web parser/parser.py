# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import datetime as dt

import sys
import time
from mongodb.mongodb_tools import *
from mongodb.mongodb_pass import ACCOUNT

available_keys = """
 [-u, --update  ] - parse new articles
 [-r, --remove  ] - delete db and re-parse articles
"""

# site url for parse
base_url = 'https://bloknot-volgograd.ru'
articles_on_page = 10
header = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/91.0.4472.77 Safari/537.36'}


def main():
    if len(sys.argv) == 1:
        print('Please, set one of the keys:', end='')
        print(available_keys)

    elif len(sys.argv) > 2:
        print('Too many keys !')

    elif sys.argv[1] not in ['-u', '--update', '-r', '--remove']:
        print('Unknown key !')

    else:
        config = get_config_collection(ACCOUNT['access_key'])
        collection = get_article_collection_(ACCOUNT['access_key'])

        if sys.argv[1] in ['-u', '--update']:
            t = time.time()
            last_id = collection.find().sort('_id', -1)[0]['_id']

            stop_url = config.find()[0]['url_to_stop']
            parse_new_articles(collection, stop_url)

            try:
                first_url = collection.find({'_id': last_id+1})[0]['article_url']
                config.update_one({'_id': 0}, {'$set': {'url_to_stop': first_url}})
            except Exception:
                print('No new articles found')

            # print parser work time
            t = time.time() - t
            print(f'\nEnd time: {int(t // 3600)} hours {int(t % 3600 // 60)} minutes')

        elif sys.argv[1] in ['-r', '--remove']:
            t = time.time()
            collection.delete_many({})

            limit = config.find()[0]['article_limit']
            parse_all_articles(collection, limit)

            first_url = collection.find()[0]['article_url']
            config.update_one({'_id': 0}, {'$set': {'url_to_stop': first_url}})

            # print parser work time
            t = time.time() - t
            print(f'\nEnd time: {int(t // 3600)} hours {int(t % 3600 // 60)} minutes')


# stop argument is a url where parser will complete the update
def parse_new_articles(collection, stop_url):
    start_id = collection.find().sort('_id', -1)[0]['_id'] + 1
    url_to_check = ''
    page = 1

    while True:
        page_url = f'{base_url}/?PAGEN_1={page}'
        articles = parse_articles_on_page(page_url, start_id, url_to_check)

        articles_urls = [article['article_url'] for article in articles]
        if stop_url in articles_urls:
            index = articles_urls.index(stop_url)
            if index != 0:
                insert_articles(collection, articles[:index])
                print(f'Parsed: {index} articles')
            break

        insert_articles(collection, articles)
        page += 1
        start_id += len(articles)
        url_to_check = articles[-1]['article_url']

        print(f'Parsed: {len(articles)} new articles')


def parse_all_articles(collection, limit=0):
    start_id = 0
    url_to_check = ''
    page = 1
    counter = limit

    while counter > 0:
        page_url = f'{base_url}/?PAGEN_1={page}'
        articles = parse_articles_on_page(page_url, start_id, url_to_check)
        insert_articles(collection, articles)

        page += 1
        start_id += len(articles)
        counter -= len(articles)
        url_to_check = articles[-1]['article_url']

        print(f'Parsed: {limit-counter}/{limit} articles')


# read html and send to BeautifulSoup.
def html_to_soup(url):
    try:
        response = requests.get(url, headers=header)
        return BeautifulSoup(response.text, 'html.parser')
    except Exception as error:
        print('Error in html_to_soup:', error.args)
        return False


def convert_string_to_dt(date_time: str):
    if 'сегодня' in date_time or 'вчера' in date_time:
        time_ = date_time.split(', ')[1].split(':')
    if 'сегодня' in date_time:
        date_time = dt.datetime.combine(dt.datetime.now(), dt.time(int(time_[0]), int(time_[1])))
    elif 'вчера' in date_time:
        date_time = dt.datetime.combine(dt.datetime.now() - dt.timedelta(days=1), dt.time(int(time_[0]), int(time_[1])))
    else:
        date_time = dt.datetime.strptime(date_time, "%d.%m.%Y %H:%M")
    return date_time


def parse_articles_on_page(page_url, start_id, url_to_check):
    # getting soup
    soup = html_to_soup(page_url)
    exit() if not soup else 0

    article_block = soup.find('ul', class_='bigline')
    articles = article_block.find_all('li')

    array = []

    # check on duplicating article
    article_url = base_url + articles[0].find('a', class_='sys')['href']
    if article_url == url_to_check:
        articles.pop(0)

    for article_id, article in enumerate(articles, start_id):
        # ссылка на статью
        article_url = base_url + article.find('a', class_='sys')['href']

        # тип
        article_type = article.find('a', class_='cat').text

        # название статьи
        article_title = article.find('a', class_='sys').text

        # дата
        article_date = article.find('span', class_='botinfo').text[1:-10]

        # кол-во комментариев
        article_comments = int(article.find('a', class_='comcount').text[1:])

        # переход на новость
        soup2 = html_to_soup(article_url)

        # дата и время
        if 'сегодня' in article_date or 'вчера' in article_date:
            article_date = article_date.replace(' в', ',')
        else:
            article_date = soup2.find('span', class_='news-date-time').text
        article_date = convert_string_to_dt(article_date)

        # текст статьи
        text = soup2.find('div', id='news-text').text
        article_text = text.translate({ord(c): '' for c in '\r\n\t\xa0'})

        # ссылка на картинку
        if soup2.find('img', class_='detail_picture'):
            img_url = 'https:' + soup2.find('img', class_='detail_picture')['src']
        else:
            img_url = None

        array.append(
            Article(
                article_id, article_url, img_url,
                article_type, article_title, article_date,
                article_text, article_comments
            ).__dict__
        )
    return array


if __name__ == '__main__':
    main()

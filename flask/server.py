from flask import Flask, render_template
import pymongo

import os
import math

app = Flask(__name__)

row_count = 10

# connect database
client = pymongo.MongoClient(os.getenv('ACCESS_KEY'))
collection = client.articles_analysis.articles


@app.route('/')
def index():
    return render_template(
        'index.html',
        attr='id=show name=eye-outline'
    )


@app.route('/<int:page>', methods=['GET', 'POST'])
def table(page):
    limit = collection.find().count()
    left, right = 'visible', 'visible'
    if page-1 in range(math.ceil(limit/10)):

        # !memory limit error after page 849
        articles = collection.find({
            '$query': {}, '$orderby': {'article_date': -1}
        })[10*(page-1):10*page]
        # .skip(10*(page-1)).limit(10)

        if page-1 == 0:
            left = 'hidden'
        elif page == math.ceil(limit/10):
            right = 'hidden'

        return render_template(
            'table.html',
            articles=articles,
            left=left,
            right=right,
            curr=10*page,
            limit=limit,
            attr='id=hide name=eye-off-outline'
        )
    else:
        return render_template(
            'index.html',
            attr='id=show name=eye-outline'
        )


if __name__ == "__main__":
    app.run()

# -*- coding: utf-8 -*-
import json
import pymongo
from mongodb.mongodb_pass import ACCOUNT


class Article:
    def __init__(
            self, _id, article_url, img_url,
            article_type, article_title, article_date,
            article_text, article_comments
    ):
        self._id = _id
        self.article_url = article_url
        self.img_url = img_url
        self.article_type = article_type
        self.article_title = article_title
        self.article_date = article_date
        self.article_text = article_text
        self.article_comments = article_comments

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)


# ------- First Student -------
def get_config_collection(key: str):
    client = pymongo.MongoClient(key)
    return client.articles_analysis.config


def get_article_collection_(key: str):
    client = pymongo.MongoClient(key)
    return client.articles_analysis.articles


def insert_articles(collection, articles: list):
    collection.insert_many(articles)


def get_article_urls(collection) -> [int, list]:
    articles = collection.find()
    urls = [article['article_url'] for article in articles]
    return len(urls), urls


# ------- Second Student -------
def get_null_facts_articles(collection) -> [int, list]:
    articles = collection.find(
        {'facts': {'$exists': False}}
    )
    return articles.count(), articles


def set_facts_in_article(collection, article_id: int, facts: list):
    text = ', '.join(facts)
    collection.update_one({'_id': article_id}, {'$set': {'facts': text}})


# ------- Third Student -------
def get_null_tonality_articles(collection) -> [int, list]:
    articles = collection.find(
        {'facts': {'$exists': True}, 'tonality': {'$exists': False}}
    )
    return articles.count(), articles


def set_tonality_in_article(collection, article_id: int, tonality: str):
    collection.update_one({'_id': article_id}, {'$set': {'tonality': tonality}})


# ------- functional test -------
def main():
    count = 0

    if ACCOUNT['access_key'] is None:
        print('[ - ] Access key is set')
        exit()
    else:
        print('[ + ] Access key is set')

    try:
        collection = get_article_collection_(ACCOUNT['access_key'])
        count = collection.count_documents({})
        print('[ + ] Connecting to the database')
    except Exception:
        print('[ - ] Connecting to the database')
        exit()

    print(f'\nFind: {count} articles in database')

    length, urls = get_article_urls(collection)
    print(urls[:length if length < 5 else 5])


if __name__ == '__main__':
    main()
